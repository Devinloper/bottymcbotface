﻿using System;

namespace BottyMcBotface_Two.Models
{
    class BirthdayEntry
    {
        public string Name { get; set; }
        public DateTime Date { get; set; }
    }
}
