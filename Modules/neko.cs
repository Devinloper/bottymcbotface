﻿using Discord.Commands;
using System.Threading.Tasks;
using BottyMcBotface_Two.Helpers;
using BottyMcBotface_Two.Enums;

namespace BottyMcBotface_Two.Modules
{
    public class neko : ModuleBase<SocketCommandContext>
    {

        [Command("neko")]
        [Alias("cat", "catto", "kitty", "kitteh")]
        [Remarks("Sends a random image of a cat.")]
        public async Task Cat()
        {
            await ReplyAsync("", false, await helpers.JsonApiCall(Context, CallTypes.CATTO));
        }
    }
}
