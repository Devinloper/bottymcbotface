﻿using BottyMcBotface_Two.Models;
using System;
using System.Collections.Generic;
using System.Timers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Discord;
using Discord.Rest;
using Discord.Commands;

namespace BottyMcBotface_Two.Modules
{
    public class resetTimer : ModuleBase<SocketCommandContext>
    {
        [Command("resetTimer")]
        [Remarks("Resets the registerd birthday timer")]
        public async Task ResetTimer()
        {
            if (Context.User.Id != 196240945736450048)
            {
                await Context.Channel.SendMessageAsync("You do not have the permission to do this!");
                return;
            }
            birthday.timer.Dispose();
            birthday.RegisterTimer();
            await ReplyAsync("Timer has been reset.", false);
            await Program.LogToFile("Birthday Timer has been reinstantiated.");
        }
    }

    public class birthday
    {
        public static Timer timer;
        private static List<BirthdayEntry> birthdayList;
        public static void RegisterTimer()
        {
            timer = new Timer(100);

            birthdayList = JsonConvert.DeserializeObject<List<BirthdayEntry>>(Program.configJson.GetValue("birthdays").ToString());

            // Trigger event on timer start, then register event to timer
            OnTimedEvent(null, null);
            timer.Elapsed += new ElapsedEventHandler(OnTimedEvent);

            timer.Enabled = true;

            timer.Interval = 86400000;
            GC.KeepAlive(timer);
        }

        private async static void OnTimedEvent(object sender, ElapsedEventArgs e)
        {
            await checkBirthdayListAsync();
        }

        public static async Task checkBirthdayListAsync()
        {
            await Program.LogToFile("Running birthday checks...");

            List<BirthdayEntry> entriesActive = birthdayList.FindAll(e => e.Date.Day == DateTime.Now.Day && e.Date.Month == DateTime.Now.Month);

            if (entriesActive.Count <= 0)
            {
                await Program.LogToFile($"{entriesActive.Count} active birthdays found. Aborting.");
                return;
            }

            await Program.LogToFile($"{entriesActive.Count} active birthdays found, preparing and sending message...");
            string activeBirthdaysString = "";
            entriesActive.ForEach(e => activeBirthdaysString += ($"[{e.Date.Date}] Wish {e.Name} a happy birtday you twat!" + System.Environment.NewLine));

            EmbedBuilder builder = new EmbedBuilder();

            builder.WithTitle("Birthday Alert!")
                .WithDescription(activeBirthdaysString)
                .WithColor(Color.Red);

            RestUser user = await Program._restClient.GetUserAsync(196240945736450048);
            RestDMChannel dmChannel = await user.CreateDMChannelAsync();
            await dmChannel.SendMessageAsync("", false, builder.Build());
        }
    }
}
