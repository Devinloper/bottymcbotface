﻿using Discord.Commands;
using System.Threading.Tasks;
using BottyMcBotface_Two.Helpers;
using BottyMcBotface_Two.Enums;

namespace BottyMcBotface_Two.Modules
{
    public class catfacts : ModuleBase<SocketCommandContext>
    {
        [Command("fact")]
        [Alias("catfact")]
        [Remarks("Sends a random fact about cats.")]
        public async Task Fact()
        {
            await ReplyAsync("", false, await helpers.JsonApiCall(Context, CallTypes.CATFACT));
        }
    }
}