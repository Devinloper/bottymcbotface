﻿using Discord.Commands;
using Discord;
using System;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace BottyMcBotface_Two.Modules
{
    public class hug : ModuleBase<SocketCommandContext>
    {
        Random rand;

        [Command("hug")]
        [Remarks("Hug someone!")]
        [Summary("mc!hug <@user>")]
        public async Task PingAsync([Optional]IUser user)
        {
            rand = new Random();
            string lonely = "themselves *;w;*";
            
            string[] hug = new string[]
            {
                "https://imgur.com/WlOmXEk.gif",
                "https://imgur.com/W987H9A.gif",
                "https://imgur.com/QCN8MUU.gif",
                "https://imgur.com/5d8OpNF.gif",
                "https://imgur.com/acetu5v.gif",
                "https://imgur.com/X8XqnT8.gif",
                "https://imgur.com/JnTbXSM.gif",
                "https://imgur.com/mZuordL.gif",
                "https://imgur.com/WvQM4p7.gif",
                "https://imgur.com/gPxYqsc.gif",
                "https://imgur.com/bn6H2uX.gif",
                "https://imgur.com/4x1E3cs.gif",
                "https://imgur.com/FtWtc4P.gif"
            };

            EmbedBuilder builder = new EmbedBuilder();

            builder.WithImageUrl(hug[rand.Next(0, hug.Length)])
                .WithColor(Color.Purple);
            
            
            if (user != null)
            {
                builder.WithDescription($"{Context.User.Mention} hugs {user.Mention}");
            }
            else
            {
                builder.WithDescription($"{Context.User.Mention} hugs {lonely}");
            }

            await ReplyAsync("", false, builder.Build());
        }
    }
}