﻿using Discord.Commands;
using Discord;
using System.Threading.Tasks;

namespace BottyMcBotface_Two.Modules
{
    public class ping : ModuleBase<SocketCommandContext>
    {
        [Command("ping")]
        [Remarks("Used to test response time")]
        public async Task PingAsync()
        {
            EmbedBuilder builder = new EmbedBuilder();
                    
            builder.WithTitle("Pong!")
                .WithDescription(Context.Client.Latency + " ms latency" + System.Environment.NewLine + "I'm connected to " + Context.Client.Guilds.Count + " servers")
                .WithColor(Color.Purple);

            await ReplyAsync("", false, builder.Build());
        }
    }
}
