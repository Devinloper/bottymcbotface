﻿using Discord.Commands;
using System.Threading.Tasks;
using BottyMcBotface_Two.Helpers;
using BottyMcBotface_Two.Enums;

namespace BottyMcBotface_Two.Modules
{
    public class inspire : ModuleBase<SocketCommandContext>
    {
        [Command("inspire")]
        [Alias("inspireme", "inspirobot", "inspiro")]
        [Remarks("Sends a randomly generated inspiring image")]
        public async Task Inspire()
        {
            await ReplyAsync("", false, await helpers.HtmlApiCall(Context, CallTypes.INSPIRE));
        }
    }
}