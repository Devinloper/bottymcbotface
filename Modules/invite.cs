﻿using Discord.Commands;
using Discord;
using System.Threading.Tasks;

namespace BottyMcBotface_Two.Modules
{
    public class invite : ModuleBase<SocketCommandContext>
    {
        [Command("invite")]
        [Alias("link")]
        [Remarks("Sends the invite link for this bot")]
        public async Task InviteAsync()
        {
            EmbedBuilder builder = new EmbedBuilder();

            builder.WithTitle("OωO you want to be friends?!")
                .WithDescription(
                    "I see that you're trying to add me! Great, I'm so happy!! Please be aware that I am in an early stage of development though." +
                    System.Environment.NewLine +
                    "https://discordapp.com/oauth2/authorize?client_id=361505785806585858&scope=bot&permissions=515399674945")
                .WithColor(Color.Gold)
                .WithThumbnailUrl("https://goo.gl/feL4Be");

            await ReplyAsync("", false, builder.Build());
        }
    }
}