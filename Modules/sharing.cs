﻿using Discord.Commands;
using Discord;
using System.Threading.Tasks;

namespace BottyMcBotface_Two.Modules
{
    public class Share : ModuleBase<SocketCommandContext>
    {

        [Command("share")]
        [Remarks("United together in friendship and labour.")]
        public async Task ShareAsync()
        {
            EmbedBuilder builder = new EmbedBuilder();

            builder.WithTitle("☭")
                .WithThumbnailUrl("https://i.imgur.com/9uPkr2v.png")
                .WithDescription("Союз нерушимый республик свободных" + System.Environment.NewLine + "https://www.marxists.org/archive/marx/works/download/pdf/Manifesto.pdf")
                .WithColor(Color.Gold);

            await ReplyAsync("", false, builder.Build());
        }
    }
}
