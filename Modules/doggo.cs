﻿using Discord.Commands;
using System.Threading.Tasks;
using BottyMcBotface_Two.Helpers;
using BottyMcBotface_Two.Enums;

namespace BottyMcBotface_Two.Modules
{
    public class doggo : ModuleBase<SocketCommandContext>
    {
        [Command("doggo")]
        [Alias("dogger", "dog", "dogg", "doge", "inu")]
        [Remarks("Sends a random image of a dog.")]
        public async Task Dog()
        {
            await ReplyAsync("", false, await helpers.JsonApiCall(Context, CallTypes.DOGGO));
        }
    }
}