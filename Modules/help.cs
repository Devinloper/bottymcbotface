using Discord.Commands;
using Discord;
using System.Threading.Tasks;

namespace BottyMcBotface_Two.Modules
{
    public class help : ModuleBase<SocketCommandContext>
    {
        [Command("help")]
        [Alias("commands")]
        [Remarks("Shows what a specific command does and what parameters it takes.")]
        public async Task HelpAsync()
        {
            string helpnew = "__Prefix: mc!__ \n";
            var dmChannel = await Context.User.CreateDMChannelAsync();

            foreach (CommandInfo item in Program._commands.Commands)
            {
                if (item.Name == "setgame" || item.Name == "guildlist" || item.Name == "resetTimer")
                {
                    continue;
                }

                if (item.Summary == null)
                {
                    helpnew += "Command: " + item.Name + System.Environment.NewLine + "Description: " + item.Remarks +
                               System.Environment.NewLine + System.Environment.NewLine;
                }
                else
                {
                    helpnew += "Command: " + item.Name + System.Environment.NewLine + "Description: " + item.Remarks +
                               System.Environment.NewLine + "Correct Usage: " + item.Summary +
                               System.Environment.NewLine +
                               System.Environment.NewLine;
                }
            }

            EmbedBuilder builder = new EmbedBuilder();

            builder.WithTitle("Help!")
                .WithDescription(helpnew)
                .WithColor(Color.Purple);

            await dmChannel.SendMessageAsync("", false, builder.Build());
            await ReplyAsync(
                "Command help has been sent to you in Private Message.");

            // https://discordapp.com/oauth2/authorize?client_id=361505785806585858&scope=bot&permissions=8
        }
    }
}
