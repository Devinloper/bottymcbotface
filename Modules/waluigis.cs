﻿using Discord.Commands;
using Discord;
using System;
using System.Threading.Tasks;

namespace BottyMcBotface_Two.Modules
{
    public class waluigis : ModuleBase<SocketCommandContext>
    {
        Random rand;

        [Command("waluigi")]
        [Remarks("Sends a random image of WALUIGIIIIII")]
        public async Task WaluigiAsync()
        {
            rand = new Random();

            string[] waluigi = new string[]
            {
                "https://i.imgur.com/JXUeHtI.png",
                "https://i.imgur.com/XU3zAjS.jpg",
                "https://i.imgur.com/aq8Qllj.png",
                "https://i.imgur.com/c9HGHay.jpg"
            };

            EmbedBuilder builder = new EmbedBuilder();

            builder.WithTitle("WAAAAAAAAAAAAAAAAAAA")
                .WithDescription("Waluigi timeeeeee")
                .WithImageUrl(waluigi[rand.Next(0, waluigi.Length)])
                .WithColor(Color.Purple);

            await ReplyAsync("", false, builder.Build());
        }
    }
}
