﻿using System.Diagnostics.CodeAnalysis;
using Discord;
using Discord.Commands;
using System.Threading.Tasks;

namespace BottyMcBotface_Two.Modules
{
    public class setgame : ModuleBase<SocketCommandContext>
    {
        [ExcludeFromCodeCoverage]
        [Command("setgame")]
        [Remarks("Sets a 'Game' for the bot")]
        public async Task GameAsync(ActivityType activityType ,[Remainder] string game)
        {
            if (Context.User.Id != 196240945736450048)
            {
                await Context.Channel.SendMessageAsync("You do not have the permission to make me play with something else!");
            }
            else
            {
                await Context.Client.SetGameAsync(game, null, activityType);
                await Context.Channel.SendMessageAsync($"Successfully changed the game to ```{activityType} {game}```");
                await Program.LogToFile($"Game was changed to {activityType} {game}");
            }
        }
    }
}
