﻿using Discord.Commands;
using System.Threading.Tasks;

namespace BottyMcBotface_Two.Modules
{
    public class guildlist : ModuleBase<SocketCommandContext>
    {
        [Command("guildlist")]
        [Remarks("List the guilds the bot is connected to")]
        public async Task PingAsync()
        {
            string guildList = "__Guild List__ \n";
            foreach (var guild in Context.Client.Guilds)
            {
                guildList += $"{guild} \n";
            }
            
            await ReplyAsync(guildList);
        }
    }
}
