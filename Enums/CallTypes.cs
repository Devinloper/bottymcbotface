﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BottyMcBotface_Two.Enums
{
    public enum CallTypes
    {
        CATFACT,
        DOGGO,
        CATTO,
        INSPIRE
    }
}
