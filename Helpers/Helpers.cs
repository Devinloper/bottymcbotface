using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using BottyMcBotface_Two.Enums;
using Discord;
using Discord.Commands;
using HtmlAgilityPack;
using Newtonsoft.Json.Linq;

namespace BottyMcBotface_Two.Helpers
{
    public class helpers
    {
        public static async Task<Embed> JsonApiCall(SocketCommandContext context, CallTypes calltype)
        {
            string websiteUrl;
            string resultIndex;
            string title;

            switch (calltype)
            {
                case CallTypes.CATFACT:
                    websiteUrl = "https://catfact.ninja/fact";
                    resultIndex = "fact";
                    title = "A neat little fact about cats!";
                    break;
                case CallTypes.DOGGO:
                    websiteUrl = "https://dog.ceo/api/breeds/image/random";
                    resultIndex = "message";
                    title = "Dog Dog Doggo";
                    break;
                case CallTypes.CATTO:
                    websiteUrl = "http://aws.random.cat/meow";
                    resultIndex = "file";
                    title = "Uhhh.. Meoww?";
                    break;
                default:
                    websiteUrl = "";
                    resultIndex = "";
                    title = "";
                    break;
            }

            await Program.LogToFile($"Making {calltype} API Call from {context.Guild.Id} || {context.Guild.Name}...");

            using (var client = new HttpClient(new HttpClientHandler
                {AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate})
            ) //This acts like a webbrowser
            { 
                client.BaseAddress = new Uri(websiteUrl); //This redirects the code to the API?
                HttpResponseMessage response = client.GetAsync("").Result; //Then it gets the information
                response.EnsureSuccessStatusCode(); //Makes sure that its successful
                string result = await response.Content.ReadAsStringAsync(); //Gets the full website
                JObject json = JObject.Parse(result); //Reads the json from the html (?)

                string fact = json[resultIndex]?.ToString(); //Saves the url to fact string

                EmbedBuilder builder = new EmbedBuilder();

                builder.WithTitle(title)
                    .WithImageUrl(fact)
                    .WithFooter($"Data from {websiteUrl}")
                    .WithColor(Color.DarkRed);

                Embed embed = builder.Build();

                return embed;
            }
        }
        
        public static async Task<Embed> HtmlApiCall(SocketCommandContext context, CallTypes calltype)
        {
            string websiteUrl;
            string nodeToSelect;
            
            switch (calltype)
            {
                case CallTypes.INSPIRE:
                    websiteUrl = @"http://inspirobot.me/api?generate=true";
                    nodeToSelect = ".";
                    break;
                default:
                    websiteUrl = "";
                    nodeToSelect = ".";
                    break;
            }
            
            await Program.LogToFile(
                $"Making Inspirobot API call from {context.Guild.Id} || {context.Guild.Name}...");

            HtmlWeb web = new HtmlWeb(); //Create HTLM client
            
            HtmlDocument htmlDoc = await web.LoadFromWebAsync(websiteUrl); //Load the url
            HtmlNode node = htmlDoc.DocumentNode.SelectSingleNode(nodeToSelect); //Select the right text

            EmbedBuilder builder = new EmbedBuilder();

            builder.WithTitle("Be inspired")
                .WithImageUrl(node.OuterHtml)
                .WithFooter("Images from inspirobot.me")
                .WithColor(Color.Green);

            Embed embed = builder.Build();
            
            return embed;
        }
    }
}
