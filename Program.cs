﻿using System;
using Discord.Commands;
using Discord;
using Discord.WebSocket;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using Discord.Rest;
using System.Text;

namespace BottyMcBotface_Two
{
    class Program
    {
        static void Main(string[] args) => new Program().RunBotAsync().GetAwaiter().GetResult();

        public static DiscordRestClient _restClient;
        public static StringBuilder sb = new StringBuilder();
        public static JObject configJson = new JObject();
        public static string commandPrefix;

        internal static CommandService _commands;

        private IServiceProvider _services;
        private static DiscordSocketClient _client;

        private static JObject ReadConfigJson()
        {
            StreamReader configFile = File.OpenText("config/conf.json");
            JsonTextReader configFileReader = new JsonTextReader(configFile);
            return (JObject)JToken.ReadFrom(configFileReader);
        }


        public async Task RunBotAsync()
        {
            var config = new DiscordSocketConfig()
            {
                GatewayIntents = GatewayIntents.AllUnprivileged
            };
            _client     = new DiscordSocketClient();
            _restClient = new DiscordRestClient();
            _commands   = new CommandService();

            _services = new ServiceCollection()
                .AddSingleton(_client)
                .AddSingleton(_restClient)
                .AddSingleton(_commands)
                .BuildServiceProvider();


            configJson = ReadConfigJson();

            commandPrefix = configJson.GetValue("prefix").ToString();

            // Get and set the bot token from the config
            string botToken = configJson.GetValue("botToken").ToString();

            // Bot Information
            await _client.SetGameAsync($"{commandPrefix}help", null, ActivityType.Watching);

            // Event subscriptions
            _client.Log += DiscordLogEvent;

            await RegisterCommandsAsync();

            await _client.LoginAsync(TokenType.Bot, botToken);
            await _restClient.LoginAsync(TokenType.Bot, botToken);

            Modules.birthday.RegisterTimer();

            await _client.StartAsync();

            await Task.Delay(-1);
        }

        private Task DiscordLogEvent(LogMessage arg)
        {
            LogToFile(arg.Message, arg.Severity.ToString(), arg.Source);

            return Task.CompletedTask;
        }

        public static Task LogToFile(string logMessage, string severity = "Info", string source = "Program")
        {
            string finalLogMessage = $"[{DateTime.Now}]    [{severity}]    [{source}]    {logMessage}";

            Console.WriteLine(finalLogMessage);
            sb.Append(finalLogMessage);

            File.AppendAllText($"{configJson.GetValue("logFile")}.txt", $"{sb} \n");

            sb.Clear();

            return Task.CompletedTask;
        }

        public async Task RegisterCommandsAsync()
        {
            _client.MessageReceived += HandleCommandAsync;

            await _commands.AddModulesAsync(Assembly.GetEntryAssembly(), _services);
        }

        public async Task HandleCommandAsync(SocketMessage arg)
        {
            if (!(arg is SocketUserMessage message) || message.Author.IsBot) return;

            int argPos = 0;

            if (message.HasStringPrefix(commandPrefix, ref argPos, StringComparison.CurrentCultureIgnoreCase) ||
                message.HasMentionPrefix(_client.CurrentUser, ref argPos))
            {
                var context = new SocketCommandContext(_client, message);

                // Only run if the command was executed in a guild
                IResult result = null;
                if (context.Guild != null)
                {
                    result = await _commands.ExecuteAsync(context, argPos, _services);
                }

                if (result == null)
                {
                    await context.Channel.SendMessageAsync("Commands can only be used in servers.");
                }
                else
                {
                    if (!result.IsSuccess)
                    {
                        if (result.ErrorReason != "Unknown command.")
                        {
                            await context.Channel.SendMessageAsync(result.ErrorReason);
                        }
                    }
                }
            }
        }
    }
}